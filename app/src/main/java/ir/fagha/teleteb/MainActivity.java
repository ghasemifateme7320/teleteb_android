package ir.fagha.teleteb;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


public class MainActivity extends AppCompatActivity implements View.OnKeyListener {
    private final String TAG = getClass().getSimpleName();
    Button login;
    Context context;
    EditText username,password;
    Socket finalMSocket;
    RequestQueue queue;
    String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;
        finalMSocket = webSocket.getWebSocketClient();
        finalMSocket.connect();
        username = findViewById(R.id.name);
        password = findViewById(R.id.password);
        login = findViewById(R.id.login);
        queue = Volley.newRequestQueue(this);
        url = "http://fagha.ir:6500/login";



        finalMSocket.on("loginFail", new Emitter.Listener() {
            @Override
            public void call(Object... args) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(context,"login failed!",Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        finalMSocket.on("loginSuccess", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.e(TAG, "call: loginSuccessResult" +args );
                Intent intent = new Intent(MainActivity.this,ChatActivity.class);
                startActivity(intent);

            }
        });
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getAccessToken();
            }
        });

        password.setOnKeyListener(this);
        username.setOnKeyListener(this);

    }

    public void login(String accessToken){
        JSONObject jsonObject= new JSONObject();
        try {

            jsonObject.put("accessToken",accessToken);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        this.finalMSocket.emit("login",jsonObject);

    }

    public void getAccessToken(){

        String finalUrl =url+"?"+"username="+username.getText().toString()+"&password="+password.getText().toString();
        JsonObjectRequest jsObjRequest = new
                JsonObjectRequest(Request.Method.GET,
                finalUrl,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            login(response.getString("accessToken"));
                            Toast.makeText(MainActivity.this,"login",Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(MainActivity.this,"fail to login",Toast.LENGTH_SHORT).show();

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse: " +error.toString());
                Toast.makeText(MainActivity.this,"That didn't work!",Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(jsObjRequest);


    }





    @Override
    public boolean onKey(View view, int keyCode, KeyEvent event) {

        if (event.getAction() == KeyEvent.ACTION_DOWN)
        {
            if (password.getText().toString().length() !=0 && username.getText().toString().length()!=0){
                switch (keyCode)
                {
                    case KeyEvent.KEYCODE_DPAD_CENTER:
                    case KeyEvent.KEYCODE_ENTER:
                        getAccessToken();

                        return true;
                    default:
                        break;}
            }
        }
        return false;
    }




}

