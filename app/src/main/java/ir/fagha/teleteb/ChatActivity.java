package ir.fagha.teleteb;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.github.nkzawa.emitter.Emitter;
public class ChatActivity extends AppCompatActivity implements View.OnKeyListener {
    Button send;
    EditText message;
    TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        message = findViewById(R.id.message);
        message.requestFocus();
        text = findViewById(R.id.text);
        send = findViewById(R.id.send);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendMessage();
            }
        });
        showNewMessage();
        message.setOnKeyListener(this);
        if (getSupportActionBar() !=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        if(id == android.R.id.home){
            webSocket.getWebSocketClient().disconnect();
            showAlertDialog();
        }
        return super.onOptionsItemSelected(item);
    }





    public void sendMessage(){
        webSocket.getWebSocketClient().emit("sendMessage", message.getText().toString());
        message.setText("");
    }

    public void showNewMessage(){
        webSocket.getWebSocketClient().on("newMessage", new Emitter.Listener() {

            @Override
            public void call(Object... args) {
                final String newMessage = (String) args[0];
                runOnUiThread(new Runnable() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void run() {
                        text.setText(text.getText().toString() + "\n\n" + newMessage);
                    }
                });
            }
        });}

    @Override
    public boolean onKey(View view, int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN)
        {
            switch (keyCode)
            {
                case KeyEvent.KEYCODE_DPAD_CENTER:
                case KeyEvent.KEYCODE_ENTER:
                    sendMessage();
                    return true;
                default:
                    break;
            }
        }
        return false;
    }
    public void showAlertDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Do you want to leave this page?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(ChatActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
        builder.setNegativeButton("No",null);
        builder.show();


    }



}


